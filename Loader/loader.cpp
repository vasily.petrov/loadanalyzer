/**
* \file loader.cpp
* \brief The application reads files and pass data to another other process (via pipes)
* \author Vasily Petrov
* \date 03.06.2018
* 
* Can be started like 'loader.exe c:\data\folder'
*/

#include <windows.h>
#include <vector>
#include <string>
#include <iostream>
#include "../Common/common.h"

using namespace std;


vector <string> fileNames; // here it doesn't matter list or vector
HANDLE g_fileNamesMutex; // for vector of fileNames
HANDLE g_pipeMutex; // lock mutex to send the second message after the first one
HANDLE g_hPipe; // TODO: it's better to pass it as argument to thread. I don't have enough time so make such dirty code

DWORD WINAPI ThreadProc(CONST LPVOID lpParam);

/**
* Main function of loader. Starts calculation.
* @param argc count of arguments+1
* @param argv arguments
*/
int main (int argc, const char* argv[]) {
	const string pattern = string(argv[1]) + "\\*";
	int status;

    if (2 != argc) {
        WRITE_ERROR("One argument (directory name) is required");
        return -1;
    }

    if (!(g_fileNamesMutex = CreateMutex(0, FALSE, NULL))) {
        WRITE_ERROR("Can't create mutex");
        return -2;
    }

    if (!(g_pipeMutex = CreateMutex(0, FALSE, NULL))) {
        status = -3;
        WRITE_ERROR("Can't create mutex");
        goto cleanup;
    }

	// TODO: what if several threads write to pipe simultaniously???
    // I don't know if it works. I think everything would work without using common mutex. But it needs to check
	vector <HANDLE> threads;

    LOG_MESSAGE("Trying to open pipe '%s'...", PIPE_NAME);

    // open pipe and write data to it
    g_hPipe = CreateFile(PIPE_NAME, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (INVALID_HANDLE_VALUE == g_hPipe)
    {
        status = -4;
        WRITE_ERROR("Can't create pipe");
		goto cleanup2;
    }

    WIN32_FIND_DATAA findData;
    HANDLE findHandle = FindFirstFileA (pattern.c_str(), &findData);
    if (INVALID_HANDLE_VALUE==findHandle) {
		status = -5;
        WRITE_ERROR("Can't enumerate files in given directory");
        goto cleanup3;
    }

    do {
        // skip . and ..
        if (!strcmp(".", findData.cFileName) || !strcmp("..", findData.cFileName))
            continue;

        // push_back name of found file to the vector
        WaitForSingleObject(g_fileNamesMutex, INFINITE);
        fileNames.push_back(string(argv[1]) + "\\"+ findData.cFileName);
        ReleaseMutex(g_fileNamesMutex);

        // create working thread
        HANDLE thread = CreateThread(NULL, 0, ThreadProc, NULL, 0, NULL);
        if (!thread)
            WRITE_ERROR("Can't create thread");
        else
            threads.push_back(thread);
    } while (FindNextFileA (findHandle, &findData));

    // Wait for all threads finished
    // the spec now guarantees vectors store their elements continuously
    WaitForMultipleObjects (threads.size(), &threads[0], TRUE, INFINITE);

	// And make cleanup
	for (vector<HANDLE>::iterator i = threads.begin(); threads.end() != i; ++i)
		CloseHandle(*i);
cleanup3:
	CloseHandle(g_hPipe);
cleanup2:
    CloseHandle(g_pipeMutex);
cleanup:
    CloseHandle(g_fileNamesMutex);
}

/**
* Main thread function. Returns the test results.
* @param lpParam params. Not used
*/
DWORD WINAPI ThreadProc(CONST LPVOID lpParam) {
    DWORD status = 0;

    // take name of found file from the vector
    WaitForSingleObject(g_fileNamesMutex, INFINITE);
    const string fileName = fileNames.back();
    fileNames.pop_back();
    ReleaseMutex(g_fileNamesMutex);

    // get file size
    HANDLE hFile = CreateFileA(fileName.c_str(), GENERIC_READ, 0, NULL,
                               OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if(INVALID_HANDLE_VALUE == hFile) {
        WRITE_ERROR("fileMappingCreate - CreateFile failed, fname = '%s'", fileName.c_str());
        return -1;
    }

    DWORD c_fileSize = GetFileSize(hFile, NULL);
    if(INVALID_FILE_SIZE == c_fileSize) {
        WRITE_ERROR("fileMappingCreate - GetFileSize failed, fname = '%s'", fileName.c_str());
        status = -2;
        goto cleanup;
    }

    // Map file to memory and then write it to pipe
    HANDLE hMapFile;
    if (!(hMapFile = CreateFileMappingA(
                 hFile,
                 NULL,                    // default security
                 PAGE_READONLY,           // read access
                 0,                       // maximum object size (high-order DWORD)
                 c_fileSize,              // maximum object size (low-order DWORD)
                 NULL))) {    // name of mapping object
        WRITE_ERROR("Can't create file mapping object for file='%s'", fileName.c_str());
        status = -3;
        goto cleanup;
    }

    LPCTSTR pBuf;
    if (!(pBuf = (LPTSTR) MapViewOfFile(hMapFile,   // handle to map object
                        FILE_MAP_READ,              // read permission
                        0,
                        0,
                        c_fileSize))) {
        WRITE_ERROR("Can't map file '%s'", fileName.c_str());
        status = -4;
        goto cleanup2;
    }

    // lock mutex to send the second message after the first one
    WaitForSingleObject(g_pipeMutex, INFINITE);
    
	// send file name
	if (!WriteFile(g_hPipe, fileName.c_str(), fileName.size(), NULL, NULL)) {
		status = -5;
		WRITE_ERROR("Can't write to pipe");
	}

	// and send its data
	if (!WriteFile(g_hPipe, pBuf, c_fileSize, NULL, NULL)) {
		status = -6;
		WRITE_ERROR("Can't write to pipe");
	}

    ReleaseMutex(g_pipeMutex);

	LOG_MESSAGE("%d bytes are written to the pipe...", c_fileSize);

    UnmapViewOfFile(pBuf);
cleanup2:
    CloseHandle(hMapFile);
cleanup:
    CloseHandle(hFile);
    return 0;
}