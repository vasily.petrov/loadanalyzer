/**
* \file analyzer.cpp
* \brief The application gets data to analyze
* \author Vasily Petrov
* \date 03.06.2018
*/

#include <windows.h>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include "../Common/common.h"

using namespace std;


#define BUFSIZE 10000 // buffer size for pipe
// NOTE: This simplistic code only allows messages up to BUFSIZE characters in length! I don't like to write something more complex

typedef pair<string, vector<unsigned char>> FileContent;
vector<FileContent> g_fileContent; // store received info here
HANDLE g_fileContentMutex; // for vector of fileNames


DWORD WINAPI InstanceThread(LPVOID lpvParam);
void Analyze(); // analyzes g_fileContent


int main (int argc, const char* argv[]) {
	int status = 0;
	vector<HANDLE> threads;

	if (!(g_fileContentMutex = CreateMutex(0, FALSE, NULL))) {
        WRITE_ERROR("Can't create mutex");
        return -1;
    }

    HANDLE hPipe = CreateNamedPipe( 
          PIPE_NAME,                // pipe name 
          PIPE_ACCESS_INBOUND,      // read access 
          PIPE_TYPE_MESSAGE |       // message type pipe 
          PIPE_READMODE_MESSAGE |   // message-read mode 
          PIPE_WAIT,                // blocking mode 
          PIPE_UNLIMITED_INSTANCES, // max. instances  
          0,                        // output buffer size 
          BUFSIZE,                  // input buffer size 
          0,                        // client time-out 
          NULL);                    // default security attribute 
    if (INVALID_HANDLE_VALUE == hPipe) {
        WRITE_ERROR("CreateNamedPipe failed.\n"); 
        status = -2;
		goto cleanup;
    }

    LOG_MESSAGE("Pipe '%ws' is created", PIPE_NAME);

    while (ConnectNamedPipe(hPipe, NULL)? TRUE : (GetLastError() == ERROR_PIPE_CONNECTED)) {
        LOG_MESSAGE("Client connected, creating a processing thread."); 
      
        // Create a thread for this client. 
        HANDLE hThread = CreateThread( 
            NULL,              // no security attribute 
            0,                 // default stack size 
            InstanceThread,    // thread proc
            (LPVOID) hPipe,    // thread parameter 
            0,                 // not suspended 
            NULL);             // doesn't return thread ID 
        if (NULL == hThread) {
            WRITE_ERROR("CreateThread failed.\n"); 
            status = -3;
			goto cleanup2;
        }

		threads.push_back(hThread);
    } 

	if (WAIT_FAILED == WaitForMultipleObjects(threads.size(), &threads[0], TRUE, INFINITE))
		WRITE_ERROR("Error in WaitForMultipleObjects");

	// now analyzer has all data and can analyze g_fileContent
	Analyze();

	for (vector<HANDLE>::iterator i = threads.begin(); threads.end() != i; ++i)
		CloseHandle(*i);
cleanup2:
    CloseHandle(hPipe);
cleanup:
	CloseHandle(g_fileContentMutex);
}

DWORD WINAPI InstanceThread(LPVOID lpvParam) {
    HANDLE hPipe = (HANDLE) lpvParam;
    char pchRequestName[BUFSIZE], pchRequestData[BUFSIZE];
    DWORD cbBytesReadName, cbBytesReadData;

    // Loop until done reading
    while (true) 
    { 
        // Read filename from the pipe. This simplistic code only allows messages up to BUFSIZE characters in length.
        BOOL fSuccess = ReadFile( 
            hPipe,        // handle to pipe 
            pchRequestName,    // buffer to receive data 
            BUFSIZE, // size of buffer 
            &cbBytesReadName, // number of bytes read 
            NULL);        // not overlapped I/O 

        if (!fSuccess || 0 == cbBytesReadName) {   
            if (ERROR_BROKEN_PIPE != GetLastError())
                WRITE_ERROR("InstanceThread: client disconnected"); 
            else
                WRITE_ERROR("InstanceThread ReadFile failed"); 
            break;
        }

		const string c_fileName(pchRequestName, cbBytesReadName);

		// Read data from the pipe. This simplistic code only allows messages up to BUFSIZE characters in length.
		fSuccess = ReadFile( 
            hPipe,        // handle to pipe 
            pchRequestData,    // buffer to receive data 
            BUFSIZE, // size of buffer 
            &cbBytesReadData, // number of bytes read 
            NULL);        // not overlapped I/O 

        if (!fSuccess || 0 == cbBytesReadData) {   
            if (GetLastError() == ERROR_BROKEN_PIPE)
                WRITE_ERROR("InstanceThread: client disconnected.\n"); 
            else
                WRITE_ERROR("InstanceThread ReadFile failed, GLE=%d.\n"); 
            break;
        }

		WaitForSingleObject(g_fileContentMutex, INFINITE);
		g_fileContent.push_back(FileContent(string(pchRequestName, cbBytesReadName),
											vector<unsigned char>(pchRequestData, pchRequestData+cbBytesReadData)));

		ReleaseMutex(g_fileContentMutex);		
    }

	DisconnectNamedPipe(hPipe);

    return 0;
}

void Analyze() {
	typedef pair<unsigned long long int /*8-byte array*/, vector<string> /*file names*/> Item;
	vector<Item> items;

    // for each found item
    for (vector<FileContent>::iterator i = g_fileContent.begin(); g_fileContent.end() != i; ++i) {
        const vector<unsigned char>& data = i->second;

        if (0 != data.size() % 8) {
            LOG_MESSAGE("Size of file needs to be a multiple of 8. Terminate working.");
            return;
        }

        for (unsigned start = 0; start < data.size(); start += 8) {
            // construct value
            long long int value = 0;
            for (int c = 0; c < 8; ++c) {
                const unsigned long long int inc = (unsigned long long int)data[start+c] << (7-c)*8;
                value += inc;
            }

            // first enumerate being constructed items vector and search for value
            bool found = false;

            for (vector<Item>::iterator j = items.begin(); items.end() != j; ++j)
                if (j->first == value) {
                    j->second.push_back(i->first);
                    found = true;
                    break;
                }
            // if nothing is found then create the new item
            if (!found)
                items.push_back(Item(value, vector<string>(1, i->first)));
        }
    }

    // Final output in the following format:
    // FFFA00BA FFFA00B1       1 [File1.bin]
    // FFFA00B1 FFFA00B2       2 [File1.bin, File2.bin]
    // FFFA00B2 FFFA00B1       1 [File2.bin]
    for (vector<Item>::iterator i = items.begin(); items.end() != i; ++i) {
        const unsigned first = i->first >> 32;
        const unsigned second = (unsigned)i->first;
        cout << hex << first << " " << second << "       " << i->second.size() << " [";
        for (vector<string>::size_type no = 0; no < i->second.size(); ++no) {
            if (0!=no) cout << ", ";
            cout << i->second.at(0);
        }
        cout << "]" << endl;
    }
}