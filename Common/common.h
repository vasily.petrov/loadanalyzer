/**
* \file common.cpp
* \brief Different usefull defines
* \author Vasily Petrov
* \date 03.06.2018
*/

#include <stdio.h>

#define PIPE_NAME L"\\\\.\\pipe\\LoaderAnalyzer"

#define WRITE_ERROR(fmt, ...) fprintf (stderr, "[%s:%d] " fmt " Error=0x%x\n", __FILE__, __LINE__, ##__VA_ARGS__, GetLastError())
#define LOG_MESSAGE(fmt, ...) printf ("[%s:%d] " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)